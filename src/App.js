import React from 'react';
import './App.css';
import Dashboard from './Dashboard/Dashboard';

const App = () => {
  //const endpoint = 'https://dev-api.zilliqa.com';

  return (
    <div className="App">
      <Dashboard/>
    </div>
  );
}


export default App;

/*
class App extends React.Component {
  ...
  }
  
  class Home extends React.Component {
  ...
  }
  
  
  class About extends React.Component {
  ...
  }
  
  
  class Contact extends React.Component {
  ...
  }
  */
