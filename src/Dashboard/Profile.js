import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Title from './Title';

const useStyles = makeStyles({
  profileContext: {
    flex: 1,
  },
});

export default function Profile({name, symbol, totalTokens}) {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title>Profile</Title>
      <Typography color="textSecondary" className={classes.depositContext}>
        {name} : {symbol}
      </Typography>
      <Typography component="p" variant="h5">
        {totalTokens}
      </Typography>
    </React.Fragment>
  );
}
