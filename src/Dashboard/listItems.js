import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PeopleIcon from '@material-ui/icons/People';
import LayersIcon from '@material-ui/icons/Layers';

export default function MainListItems(props) {

  console.log('MainListItem props', props);

  const onOverview = () => {
    console.log('onOverviewClick');
    props.onOverviewClick();
  };
  const onProfile = () => {
    console.log('onProfile');
    props.onProfileClick();
  };
  const onHolders = () => {
    console.log('onHolders');
    props.onHoldersClick();
  }
  
  return (
    <div>
      <ListItem button onClick={onOverview}>
        <ListItemIcon>
          <DashboardIcon />
        </ListItemIcon>
        <ListItemText primary="Overview" />
      </ListItem>
      <ListItem button onClick={onProfile}>
        <ListItemIcon>
          <LayersIcon />
        </ListItemIcon>
        <ListItemText primary="Profile" />
      </ListItem>
      <ListItem button onClick={onHolders}>
        <ListItemIcon>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText primary="Holders" />
      </ListItem>
    </div>
  )
}