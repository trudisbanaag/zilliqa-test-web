import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
//import SearcItemIcon from '@material-ui/core/ListItemSecondaryAction';
import MainListItems from './listItems';
import Profile from './Profile';
import Holders from './Holders';
import Zilliqa from '../Zilliqa';

const drawerWidth = 240;

const TOKEN_ID = '5938fc8af82250ad6cf1da3bb92f4aa005cb2717';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  ProfileHeight: {
    height: 150,
  },
}));

export default function Dashboard() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  const fixedHeightPaper = clsx(classes.paper, classes.ProfileHeight);

  /* open tabs state */
  const [profileIsOpen, setProfileIsOpen] = React.useState(true);
  const [holdersIsOpen, setHoldersIsOpen] = React.useState(true);

  /* Profile (tokenInfo) state */
  const [tokenInfo, setTokenInfo] = React.useState({
    name: 'N/A',
    symbol: 'N/A',
    totalTokens: '0',
  });

  /* Holders (users) state */
  const [ holders, setHolders ] = React.useState([]);

  /* Search input state  */
  const [ searchText, setSearchText ] = React.useState('');


  const onOverviewClick = async () => {
    console.log('Dashboard onOverviewClick');
    setProfileIsOpen(true);
    setHoldersIsOpen(true);

    try {
      setTokenInfo({ totalTokens: 'Loading...' });
      const [supply, info] = await Zilliqa.getTokenInfo(TOKEN_ID);
      setTokenInfo({
        name: info.data.name,
        symbol: info.data.symbol,
        totalTokens: supply.data.total_tokens
      });

      const response = await Zilliqa.getUsers(TOKEN_ID);
      const keys = Object.keys(response.data.users);
      const rows = [];
      for (let i = 0; i < keys.length; i++) {
        const rank = i+1;
        const address = keys[i];
        const balance = response.data.users[address];
        const row = { rank, address, balance };
        rows.push(row);
      }
      setHolders(rows);
    } catch (error) {
      console.log('ERROR', error);
      console.log('TODO');
    }
  };

  const onProfileClick = async () => {
    console.log('Dashboard onProfileClick');
    setProfileIsOpen(true);
    setHoldersIsOpen(false);
    try {
      setTokenInfo({ totalTokens: 'Loading...' });
      const [supply, info] = await Zilliqa.getTokenInfo(TOKEN_ID);
      setTokenInfo({
        name: info.data.name,
        symbol: info.data.symbol,
        totalTokens: supply.data.total_tokens
      });
    } catch (error) {
      console.log('ERROR', error);
      console.log('TODO');
    }
  };

  const onHoldersClick = async () => {
    console.log('Dashboard onHolderClick');
    setHoldersIsOpen(true);
    setProfileIsOpen(false);
    try {
      const response = await Zilliqa.getUsers(TOKEN_ID);
      const keys = Object.keys(response.data.users);
      const rows = [];
      for (let i = 0; i < keys.length; i++) {
        const rank = i+1;
        const address = keys[i];
        const balance = response.data.users[address];
        const row = { rank, address, balance };
        rows.push(row);
      }
      setHolders(rows);
    } catch (error) {
      console.log('ERROR', error);
      console.log('TODO');
    }
  };

  const onAddressSearch = async (ev) => {
    console.log('Dashboard onAddressSearch');
    console.log(searchText);
    try {
      const response = await Zilliqa.getAddressBalance(TOKEN_ID, searchText);
      const rows = [];
      const balance = response.data.balance;
      const rank = 0;
      const address = searchText
      const row = { rank, address, balance };
      rows.push(row);
      
      setHolders(rows);

    } catch (error) {
      console.log('ERROR', error);
      console.log('TODO');
    }
  };

  const onInputChange = (ev) => {
    console.log(ev.target.value)
    setSearchText(ev.target.value);
  } 

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            CoinScan
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>
          <MainListItems 
            onOverviewClick={onOverviewClick}
            onProfileClick={onProfileClick}
            onHoldersClick={onHoldersClick}
          />
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3}>

            {/* Profile */}
            { profileIsOpen &&
              <Grid item xs={12}>
                <Paper className={fixedHeightPaper}>
                  <Profile 
                    name={tokenInfo.name}
                    symbol={tokenInfo.symbol}
                    totalTokens={tokenInfo.totalTokens}
                  />
                </Paper>
              </Grid>
            }

            {/* Holders */}
            { holdersIsOpen &&
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <Holders 
                    rows={holders}
                  />
                  { !profileIsOpen &&
                  <Paper> 
                     SEARCH <input type="text" onChange={onInputChange} size="32"/>
                     <Button variant="contained" onClick={onAddressSearch}>
                       GO
                     </Button>  
                   </Paper>
                  }   
                </Paper>
              </Grid>
            }
          </Grid>
        </Container>
      </main>
    </div>
  );
}
