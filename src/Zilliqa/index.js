import axios from 'axios';

const BASE_URL = 'http://localhost:3000';

class Zilliqa {

  static async getTokenInfo(tokenId) {
    console.log('getTokenInfo', tokenId);
    const supply = axios.get(`${BASE_URL}/tokens/${tokenId}/supply`);
    const info = axios.get(`${BASE_URL}/tokens/${tokenId}/info`);
    return Promise.all([supply, info]);
  }

  static async getUsers(tokenId) {
    console.log('getUsers tokenId', tokenId);
    return axios.get(`${BASE_URL}/tokens/${tokenId}/users`);
  }

  static async getAddressBalance(tokenId, address) {
    console.log('getAddressBalance tokenId', tokenId);
    console.log('getAddressBalance address', address);

    return axios.get(`${BASE_URL}/tokens/${tokenId}/${address}`);
  }
}

export default Zilliqa;
