#  Zilliqa Test web component
## Quick Start

### Install:

    git clone https://bitbucket.org/trudisbanaag/zilliqa-test-web

### Install npm packages:

```bash
npm install
```

### Run it:

```bash
npm start
```

You can access the app in `http://localhost:3000`.

## Pointing to Zillaqa Test API

By default, the proxy points to http://localhost:3001. If you want to point it to another endpoint, please update the src/Zilliqa/index.js. Please set the BASE_URL to the appropriate endpoint.
